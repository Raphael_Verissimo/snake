package com.verissimo.classicsnake;

import org.junit.Test;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 */
public class SnakeUnitTest {

    @Test
    public void createSnake() {
        Snake snake = new Snake(100, 400, 1);

        assertEquals(3, snake.getSnakeSize());
    }

    @Test
    public void addSnakeBody() {
        Snake snake = new Snake(100, 400, 1);

        snake.addSnakeBody();
        snake.addSnakeBody();
        snake.addSnakeBody();
        snake.addSnakeBody();

        assertEquals(7, snake.getSnakeSize());
    }

    @Test
    public void moveSnake() {
        Snake snake = new Snake(100, 400, 1);
        assertEquals(Snake.SnakeDirection.DOWN, snake.getSnakeDirection());

        snake.setLastMoveDistance(10);
        snake.moveLeft();
        assertEquals(Snake.SnakeDirection.DOWN, snake.getSnakeDirection());

        snake.setLastMoveDistance(100);
        snake.moveLeft();
        assertEquals(Snake.SnakeDirection.LEFT, snake.getSnakeDirection());

        snake.setLastMoveDistance(100);
        snake.moveUp();
        assertEquals(Snake.SnakeDirection.UP, snake.getSnakeDirection());

        snake.setLastMoveDistance(100);
        snake.moveRight();
        assertEquals(Snake.SnakeDirection.RIGHT, snake.getSnakeDirection());

        snake.setLastMoveDistance(100);
        snake.moveDown();
        assertEquals(Snake.SnakeDirection.DOWN, snake.getSnakeDirection());

        snake.setLastMoveDistance(100);
        snake.moveUp();
        assertEquals(Snake.SnakeDirection.DOWN, snake.getSnakeDirection());
    }
}