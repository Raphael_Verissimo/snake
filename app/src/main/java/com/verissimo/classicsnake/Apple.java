package com.verissimo.classicsnake;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.Random;

public class Apple implements GameObject {

    private Bitmap apple;
    private Point applePosition;
    private int appleSize;
    private Rect appleRect;
    private int appleColor;
    private boolean hasCreated;

    public Apple(Context context) {
        appleSize = 55;
        hasCreated = false;
        int id = R.drawable.apple;
        Resources res = context.getResources();
        apple = BitmapFactory.decodeResource(res, id);
    }

    public int getAppleSize(){
        return appleSize;
    }

    public boolean hasCollidedWithApple(Rect object){
        if(appleRect.intersects(object.left, object.top, object.right, object.bottom))
            return true;

        return false;
    }

    public void createApple(Point point, int color){
        appleRect = new Rect(point.x, point.y, point.x + getAppleSize(), point.y + getAppleSize());
        applePosition = new Point(point.x, point.y);
        this.appleColor = color;
        hasCreated = true;
    }

    public void destroyApple(){
        hasCreated = false;
    }

    @Override
    public void draw(Canvas canvas) {
        if (hasCreated)
        {
            Paint paint = new Paint();
            paint.setColor(appleColor);
            canvas.drawBitmap(apple, applePosition.x, applePosition.y, paint);

        }
    }

    @Override
    public void update() {

    }
}
