package com.verissimo.classicsnake;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Level implements GameObject {

    private Rect topWall;
    private Rect bottomWall;
    private Rect leftWall;
    private Rect rightWall;

    private boolean hasCreated;

    public Level (){
        hasCreated = false;
    }

    public boolean hasCollidedWithWalls(Rect object){
        if(hasCreated) {
            if(leftWall.intersects(object.left, object.top, object.right, object.bottom) ||
               rightWall.intersects(object.left, object.top, object.right, object.bottom) ||
               topWall.intersects(object.left, object.top, object.right, object.bottom) ||
               bottomWall.intersects(object.left, object.top, object.right, object.bottom)){
                return true;
            }
        }

        return false;
    }

    @Override
    public void draw(Canvas canvas) {

        if(!hasCreated)
            createLevel(canvas);

        Paint paint = new Paint();
        paint.setColor(Color.rgb(150, 0, 50));
        canvas.drawRect(topWall, paint);
        canvas.drawRect(leftWall, paint);
        canvas.drawRect(bottomWall, paint);
        canvas.drawRect(rightWall, paint);
    }

    @Override
    public void update() {

    }

    private void createLevel(Canvas canvas){
        hasCreated = true;
        int wallSize = 20;
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        topWall = new Rect(0, 0, width, wallSize);
        leftWall = new Rect(0, 0, wallSize, height);
        bottomWall = new Rect(0, height - wallSize, width, height);
        rightWall = new Rect(width - wallSize, 0, width, height);
    }
}
