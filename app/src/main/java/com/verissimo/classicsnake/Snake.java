package com.verissimo.classicsnake;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

public class Snake implements GameObject {

    private Rect head;
    private int color;
    private SnakeDirection headDirection;
    private int speed;
    private boolean isAlive;
    private int snakeWidth;

    private List<Rect> body;
    private List<SnakeDirection> directions;
    private int lastMoveDistance;
    private final int MIN_BODY_SIZE = 3;

    public Snake(int positionX, int positionY, int color){

        if (body != null)
            throw new IllegalStateException("Snake Already Created !");

        this.head = new Rect(positionX, positionY, 150, 450);
        this.color = color;
        headDirection = SnakeDirection.DOWN;
        snakeWidth = 50;
        speed = 3;
        lastMoveDistance = 0;
        isAlive = true;

        createBody();
    }

    public void setLastMoveDistance(int value) {lastMoveDistance = value;}

    public SnakeDirection getSnakeDirection() {return headDirection;}

    public Rect getSnakeHeadRect() { return head; }

    public int getSnakeSize(){
        return body.size();
    }

    public int getSnakeWidth(){
        return snakeWidth;
    }

    public void setColor(int color){
        this.color = color;
    }

    public void killSnake(){
        isAlive = false;
        setColor(Color.rgb(255, 0, 0));
    }

    public void moveUp(){
        if(lastMoveDistance > getSnakeWidth()*1.1f){
            if (headDirection != SnakeDirection.DOWN) {
                lastMoveDistance = 0;
                headDirection = SnakeDirection.UP;
            }
        }
    }

    public void moveDown(){
        if(lastMoveDistance > getSnakeWidth()*1.1f) {
            if (headDirection != SnakeDirection.UP) {
                lastMoveDistance = 0;
                headDirection = SnakeDirection.DOWN;
            }
        }
    }

    public void moveLeft(){
        if(lastMoveDistance > getSnakeWidth()*1.1f) {
            if (headDirection != SnakeDirection.RIGHT){
                lastMoveDistance = 0;
                headDirection = SnakeDirection.LEFT;
            }
        }
    }

    public void moveRight(){
        if(lastMoveDistance > getSnakeWidth()*1.1f) {
            if (headDirection != SnakeDirection.LEFT) {
                lastMoveDistance = 0;
                headDirection = SnakeDirection.RIGHT;
            }
        }
    }

    public void addSnakeBody(){

        SnakeDirection lastDirection = directions.get(directions.size()-1);
        Rect lastRect = body.get(body.size()-1);
        Rect nextRect = new Rect();

        switch (lastDirection){
            case UP:
                nextRect = new Rect(lastRect.left, lastRect.top + getSnakeWidth() + 1, lastRect.right, lastRect.bottom + getSnakeWidth() + 1);
                break;

            case DOWN:
                nextRect = new Rect(lastRect.left, lastRect.top - getSnakeWidth() - 1, lastRect.right, lastRect.bottom - getSnakeWidth() -1);
                break;

            case LEFT:
                nextRect = new Rect(lastRect.left + getSnakeWidth() + 1, lastRect.top, lastRect.right + getSnakeWidth() + 1, lastRect.bottom);
                break;

            case RIGHT:
                nextRect = new Rect(lastRect.left - getSnakeWidth() - 1, lastRect.top, lastRect.right - getSnakeWidth() - 1, lastRect.bottom);
                break;
        }

        body.add(nextRect);
        directions.add(lastDirection);
    }

    public void checkAutoColision(){
        if(body.size() > MIN_BODY_SIZE){
            for (int bodyIndex = MIN_BODY_SIZE ; bodyIndex < body.size() ; bodyIndex++){

                if(body.get(bodyIndex).intersects(head.left, head.top, head.right, head.bottom)){
                    killSnake();
                }
            }
        }
    }

    public boolean hasColidedWithSnake(Rect object){
       if(head.intersects(object.left, object.top, object.right, object.bottom)) {
           return true;
       }

       for (Rect bodyRect : body){
            if(bodyRect.intersects(object.left, object.top, object.right, object.bottom)){
                return true;
            }
       }

       return false;
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(head, paint);

        for (Rect snakeBody : body) {
            canvas.drawRect(snakeBody, paint);
        }
    }

    @Override
    public void update() {

    }

    public void updateSnake () {
        if (isAlive){
            moveSnakeBody(head, headDirection, -1);
            lastMoveDistance += speed;

            for (int bodyIndex = 0 ; bodyIndex < body.size() ; bodyIndex++) {
                moveSnakeBody(body.get(bodyIndex), directions.get(bodyIndex), bodyIndex);
            }

            checkAutoColision();
        }
    }

    private void moveSnakeBody(Rect rect, SnakeDirection currentDirection, int bodyIndex){
        Point destinationPoint = new Point();
        destinationPoint.set(rect.centerX(), rect.centerY());

        switch (currentDirection){
            case UP:
                destinationPoint.set(destinationPoint.x, destinationPoint.y - speed);
                break;

            case DOWN:
                destinationPoint.set(destinationPoint.x, destinationPoint.y + speed);
                break;

            case LEFT:
                destinationPoint.set(destinationPoint.x - speed, destinationPoint.y);
                break;

            case RIGHT:
                destinationPoint.set(destinationPoint.x + speed, destinationPoint.y);
                break;
        }

        rect.set(destinationPoint.x - rect.width()/2, destinationPoint.y - rect.height()/2, destinationPoint.x + rect.width()/2,destinationPoint.y + rect.height()/2 );

        SnakeDirection nextDirection;
        Point nextPoint;
        if (bodyIndex > -1){
            if (bodyIndex == 0) {
                nextDirection = headDirection;
                nextPoint = new Point(head.centerX(), head.centerY());
            } else {
                nextDirection = directions.get(bodyIndex-1);
                nextPoint = new Point(body.get(bodyIndex-1).centerX(), body.get(bodyIndex-1).centerY());
            }

            float horizontalDistance = (float)(rect.exactCenterX() - nextPoint.x);
            float verticalDistance = (float)(rect.exactCenterY() - nextPoint.y);

            if (currentDirection != nextDirection)
            {
                switch (nextDirection){
                    case UP:
                    case DOWN:
                        if (Math.abs(horizontalDistance) < 0.5f)
                            directions.set(bodyIndex, nextDirection);
                        break;

                    case LEFT:
                    case RIGHT:
                        if (Math.abs(verticalDistance) < 0.5f)
                            directions.set(bodyIndex, nextDirection);
                        break;
                }
            }
        }
    }

    private void createBody(){
        body = new ArrayList<Rect>();
        directions = new ArrayList<SnakeDirection>();

        Rect body1 = new Rect(this.head.left, this.head.top - 51, this.head.right, this.head.bottom - 51);
        Rect body2 = new Rect(this.head.left, this.head.top - 102, this.head.right, this.head.bottom - 102);
        Rect body3 = new Rect(this.head.left, this.head.top - 153, this.head.right, this.head.bottom - 153);

        body.add(body1);
        directions.add(SnakeDirection.DOWN);

        body.add(body2);
        directions.add(SnakeDirection.DOWN);

        body.add(body3);
        directions.add(SnakeDirection.DOWN);
    }

    private boolean hasCollidedWhitHead(float positionX, float positionY, int objectSize){

       if ((positionX + objectSize/2 > head.left && positionX + getSnakeWidth()/2 < head.left + getSnakeWidth()) ||
           (positionX - objectSize/2 > head.left && positionX - getSnakeWidth()/2 < head.left + getSnakeWidth()))
        {
            if ((positionY + objectSize/2 > head.top && positionY + getSnakeWidth()/2 < head.top + getSnakeWidth()) ||
                    (positionY - objectSize/2 > head.top && positionY - getSnakeWidth()/2 < head.top + getSnakeWidth())){
                return true;
            }
        }

        return false;
    }

    private boolean hasCollidedWhitBodyRect(Rect bodyRect, float positionX, float positionY, int objectSize){

        if ((positionX + objectSize/2 > bodyRect.left && positionX + getSnakeWidth()/2 < bodyRect.left + getSnakeWidth()) ||
                (positionX - objectSize/2 > bodyRect.left && positionX - getSnakeWidth()/2 < bodyRect.left + getSnakeWidth()))
        {
            if ((positionY + objectSize/2 > bodyRect.top && positionY + getSnakeWidth()/2 < bodyRect.top + getSnakeWidth()) ||
                    (positionY - objectSize/2 > bodyRect.top && positionY - getSnakeWidth()/2 < bodyRect.top + getSnakeWidth())){
                return true;
            }
        }

        return false;
    }

    public enum SnakeDirection {
        UP, DOWN, LEFT, RIGHT
    }
}
