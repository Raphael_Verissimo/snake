package com.verissimo.classicsnake;

import android.graphics.Color;
import android.graphics.Point;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;

    private Snake snake;
    private Level level;
    private Apple apple;

    private final float minimunDrag = 3;

    private float lastX = 0;
    private float lastY = 0;

    private boolean hasStarted;
    private int canvasWidth;
    private int canvasHeight;

    public GamePanel (Context context){
        super(context);
        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        snake = new Snake(100, 400, Color.rgb(0, 100, 0));
        level = new Level();
        apple = new Apple(context);
        apple.createApple(new Point(500, 600), Color.RED);

        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed (SurfaceHolder holder) {
        boolean retry = true;

        while (true) {
            try{
                thread.setRunning(false);
                thread.join();
            } catch (Exception e){e.printStackTrace();}
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                lastX = event.getX();
                lastY = event.getY();
            case MotionEvent.ACTION_MOVE:
                float deltaX = lastX - event.getX();
                float deltaY = lastY - event.getY();

                if (Math.abs(deltaY) > minimunDrag || Math.abs(deltaX) > minimunDrag) {

                    if (Math.abs(deltaY) > Math.abs(deltaX)) {
                        if (deltaY > 0) {
                           snake.moveUp();
                        }

                        if (deltaY < 0) {
                            snake.moveDown();
                        }
                    } else {
                        if (deltaX > 0) {
                            snake.moveLeft();
                        }

                        if (deltaX < 0) {

                            snake.moveRight();
                        }
                    }
                }
        }

        return true;
    }

    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);

        if (!hasStarted){
            canvasWidth = canvas.getWidth();
            canvasHeight = canvas.getHeight();
        }

        canvas.drawColor(Color.WHITE);

        snake.draw(canvas);
        level.draw(canvas);
        apple.draw(canvas);
    }

    public void update() {
        snake.updateSnake();
        updateLevelControls();
    }

    private void updateLevelControls(){
        if(level.hasCollidedWithWalls(snake.getSnakeHeadRect())){
            snake.killSnake();
        }

        if (apple.hasCollidedWithApple(snake.getSnakeHeadRect())){
            apple.destroyApple();
            snake.addSnakeBody();
            spawnRandomApple();
        }
    }

    private void spawnRandomApple(){
        double random = Math.random();

        int nextPointX = ((int) (random * canvasWidth));
        int nextPointY = ((int) (random * canvasWidth));

        Rect nextRect = new Rect(nextPointX, nextPointY, nextPointX + apple.getAppleSize(), nextPointY + apple.getAppleSize());

        if ((level.hasCollidedWithWalls(nextRect)) ||
           (snake.hasColidedWithSnake(nextRect)))
        {
            spawnRandomApple();
        } else {
            apple.createApple(new Point(nextPointX, nextPointY), Color.WHITE);
        }
    }
}
